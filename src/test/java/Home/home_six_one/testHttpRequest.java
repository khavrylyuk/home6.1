package Home.home_six_one;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.testng.Assert;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class testHttpRequest {
	
private OkHttpClient client;
	
	@BeforeMethod(alwaysRun = true)
	public void setupHttpClient() {
		client = new OkHttpClient();
	}
 
	@Test
	public void httpTest() throws IOException {
		Request request = new Request.Builder().url("https://www.google.com/").build();
		Response response = client.newCall(request).execute();
		Assert.assertEquals(response.code(), 200, "Status code was not 200");
		System.out.println("Responce code: " + response.code());
		Assert.assertTrue(response.header("content-type").contains("text/html"), "content-type is not text/html");
		System.out.println("Content-type: text/html");
		String responseBody = response.body().string();
		assertTrue(responseBody.contains("hplogo"), "Element 'hplogo' is not found");
		
		
	}

}
